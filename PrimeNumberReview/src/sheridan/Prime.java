package sheridan;

public class Prime {
	public static boolean isPrime( int number ) {
		for ( int i = 2 ; i <= number /2 ; i ++) {
			if ( number % i == 0  ) {
				return false;
			}
		}
		return true;
 	}

	public static void main(String[] args) {
		

		System.out.println( "Number 29 is prime? " + Prime.isPrime( 29 ) );
		System.out.println( "Number 29 is prime? " + Prime.isPrime( 30 ) );		
		System.out.println( "Number 33 is prime? " + Prime.isPrime( 33 ) );
		System.out.println( "Number 34 is prime? " + Prime.isPrime( 34 ) );	
		System.out.println( "Number 35 is prime? " + Prime.isPrime( 35 ) );		
		System.out.println( "Number 36 is prime? " + Prime.isPrime( 36 ) );		
		System.out.println( "Number 3 is prime? " + Prime.isPrime( 3 ) ); // Jasmeet Singh
		System.out.println( "Number 5 is prime? " + Prime.isPrime( 5  ) ); // Ravneet Raina
		System.out.println( "Number 11 is prime? " + Prime.isPrime( 11 ) ); // Jasmeet Singh
		System.out.println( "Number 21 is prime? " + Prime.isPrime( 21 ) ); // Ravneet Raina
		
	}

}
